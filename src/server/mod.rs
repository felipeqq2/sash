mod html;
mod middleware;

use std::{net::IpAddr, str::FromStr, sync::Arc};

use axum::{http::header, middleware::from_fn, routing::get, Router};

use crate::config::AppConfig;

pub async fn start(config: AppConfig) {
    let port = config.port.unwrap_or(9000);
    let address = (IpAddr::from_str("127.0.0.1").unwrap(), port).into();

    eprintln!("Starting server at http://{}", address);

    let shared_config = Arc::new(config);
    let app = Router::new()
        .route("/", get(html::get_root))
        .route(
            "/favicon.svg",
            get(|| async { ([(header::CONTENT_TYPE, "image/svg+xml")], FAVICON) }),
        )
        .route(
            "/styles.css",
            get(|| async { ([(header::CONTENT_TYPE, "text/css")], CSS) }),
        )
        .with_state(shared_config)
        .layer(from_fn(middleware::middleware));

    axum::Server::bind(&address)
        .serve(app.into_make_service())
        .await
        .unwrap();
}

static FAVICON: &str = include_str!("favicon.svg");
static CSS: &str = include_str!("styles.css");
