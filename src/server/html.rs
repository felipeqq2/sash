use std::sync::Arc;

use axum::{extract::State, http::StatusCode};
use maud::{html, Markup, DOCTYPE};

use crate::config::AppConfig;

pub async fn get_root(State(config): State<Arc<AppConfig>>) -> Markup {
    html! {
        (header())
        ul {
            @for link in &config.links {
                li {
                    a href=(link.url) {
                        div .link {
                            @if let Some(icon) = &link.icon {
                                img src=(icon);
                            }
                            div {
                                div .name { (link.name) }
                                div .url { (link.url) }
                            }
                        }
                    }
                }
            }
        }
    }
}

pub async fn get_error(code: &StatusCode) -> Markup {
    html! {
        (header())
        h1 { (code) }
    }
}

fn header() -> Markup {
    html! {
        (DOCTYPE)
        meta charset="utf-8";
        title { "SASH!" }
        link rel="icon" href="favicon.svg";
        link rel="stylesheet" href="styles.css";
        h1 { "SASH" }
    }
}
