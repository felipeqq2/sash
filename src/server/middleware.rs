use axum::{
    http::{Request, StatusCode},
    middleware::Next,
    response::{IntoResponse, Response},
};

pub async fn middleware<B>(request: Request<B>, next: Next<B>) -> Response {
    eprintln!("{} request at {}", request.method(), request.uri());
    let response = next.run(request).await;
    let status = response.status();

    if !StatusCode::is_success(&status) {
        eprintln!("Code {}", status);
        super::html::get_error(&status).await.into_response()
    } else {
        response
    }
}
