mod config;
mod server;

fn main() -> std::io::Result<()> {
    let loaded_config = config::AppConfig::get()?;

    eprintln!("Config is loaded!");

    let rt = tokio::runtime::Runtime::new()?;
    eprintln!("Starting up Tokio runtime...");

    rt.block_on(server::start(loaded_config));

    Ok(())
}
