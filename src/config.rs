use directories::ProjectDirs;
use serde::{Deserialize, Serialize};

use std::fs::File;
use std::io;
use std::path::PathBuf;

#[derive(Serialize, Deserialize, Debug)]
pub struct AppConfig {
    pub port: Option<u16>,
    pub links: Vec<Link>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Link {
    pub url: String,
    pub name: String,
    pub icon: Option<String>,
}

impl AppConfig {
    fn get_path() -> PathBuf {
        let default_file = match ProjectDirs::from("", "", "sash") {
            None => PathBuf::from("~/.config/sash/config.json"),
            Some(p) => p.config_local_dir().join("config.json"),
        };

        match std::env::var("SASH_CONFIG") {
            Ok(path) => PathBuf::from(path),
            Err(_) => default_file,
        }
    }

    pub fn get() -> io::Result<Self> {
        let path = Self::get_path();
        eprintln!("Loading configuration at {:?}", path);

        Ok(serde_json::from_reader(File::open(path)?)?)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn check_config_env() {
        let with_var = PathBuf::from("/my/file.json");

        assert_ne!(AppConfig::get_path(), with_var);
        std::env::set_var("SASH_CONFIG", "/my/file.json");
        assert_eq!(AppConfig::get_path(), with_var);
    }
}
