# Sash.
(a **s**imple d**ash**board)

Because I'm tired of having to remember the ports of everything that's running on my machine, and don't want to run yet another heavy service.

![Sash screenshot](./screenshot.png)

## Installation
If you have `cargo` installed, just run `cargo install sash`. No precompiled binaries are available at the time.

A personal *Nix* derivation for this package is available [here](https://codeberg.org/felipeqq2/.dotfiles/src/branch/main/overlays/sash.nix), and a systemd service is [here](https://codeberg.org/felipeqq2/.dotfiles/src/branch/main/modules/sash.nix). Be free to copy it.

## Configuration
You can add a JSON file at `$XDG_CONFIG_HOME/sash/config.json` (or `~/.config/sash/config.json` — check out [directories](https://lib.rs/crates/directories) for Win/Mac) according to the example:

```json
{
  "port": 1234,
  "links": [
    {
      "name": "IPFS",
      "url": "http://localhost:5001/webui",
      "icon": "https://cdn.jsdelivr.net/gh/walkxcode/dashboard-icons/svg/ipfs.svg"
    },
    {
      "name": "Syncthing",
      "url": "http://localhost:8384"
    }
  ]
}
```

The default `port` is 9000, the `icon` link is optional, and remember to avoid trailing commas or it will yell at you.

You can change the config file path with the environment variable `SASH_CONFIG`.
